package com.eleks.bookroom.data.serviceExecutor;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by maryan.melnychuk on 04.02.14.
 */
public interface RBServiceCallbackListener {
    void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data);

}
