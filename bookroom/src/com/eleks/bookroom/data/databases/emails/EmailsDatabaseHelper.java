package com.eleks.bookroom.data.databases.emails;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maryan.melnychuk on 02.01.14.
 */
public class EmailsDatabaseHelper extends SQLiteAssetHelper {
    public static final String DB_NAME = "emailsDB";
    public static final String TABLE_NAME = "emails";
    public static final String EMAIL_ROW = "email";
    public static final String ID_ROW = "_id";

    public EmailsDatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if(oldVersion != newVersion){
            //sqLiteDatabase.execSQL("DROP DATABASE " + DB_NAME + ";");
        }
    }

    public void updateEmails(List<String> emails){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try{
            Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            if(cursor.getCount() > 0){
                deleteEmails(db);
            }
            addEmails(emails, db);

            cursor.close();
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public void addEmails(List<String> emails){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for(String email : emails){
            contentValues.put(EMAIL_ROW, email);
            db.insert(TABLE_NAME, null, contentValues);
        }
        db.close();
    }

    public void addEmails(List<String> emails, SQLiteDatabase db){
        ContentValues contentValues = new ContentValues();
        for(String email : emails){
            contentValues.put(EMAIL_ROW, email);
            db.insert(TABLE_NAME, null, contentValues);
        }
    }

    public void deleteEmails(){
        SQLiteDatabase db = this.getWritableDatabase();
        String deleteTableData = "DELETE FROM " + TABLE_NAME;
        db.execSQL(deleteTableData);
        db.close();
    }

    public void deleteEmails(SQLiteDatabase db){
        String deleteTableData = "DELETE FROM " + TABLE_NAME;
        db.execSQL(deleteTableData);
    }

    public List<String> getEmails(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        List<String> emailsList = new ArrayList<String>();
        while (cursor.moveToNext()){
            emailsList.add(cursor.getString(cursor.getColumnIndex(EMAIL_ROW)));
        }
        cursor.close();
        db.close();
        return emailsList;
    }

    public Cursor getDescriptionsFor(String vehicle, SQLiteDatabase db) {
        String[] projection = new String[]{ID_ROW, EMAIL_ROW};
        String[] selectionArgs = new String[]{"%" + vehicle + "%"};


        String sortOrder = EMAIL_ROW;

        StringBuilder select = new StringBuilder(EMAIL_ROW)
                            .append(" LIKE ? ");


        return db.query(TABLE_NAME, projection, select.toString(), selectionArgs, null, null, sortOrder);
    }
}
