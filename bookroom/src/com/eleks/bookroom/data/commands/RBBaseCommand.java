package com.eleks.bookroom.data.commands;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

import com.eleks.bookroom.data.RBApplication;

/**
 * Created by maryan.melnychuk on 05.02.14.
 */
public abstract class RBBaseCommand implements Parcelable {
    public static String EXTRA_PROGRESS = RBApplication.PACKAGE.concat(".EXTRA_PROGRESS");
    public static final int RESPONSE_SUCCESS = 0;
    public static final int RESPONSE_FAILED = 1;
    public static final int RESPONSE_PROGRESS = 2;

    private ResultReceiver mResultCallback;

    protected volatile boolean cancelled = false;

    public final void execute(Intent intent, Context context, ResultReceiver callback){
        mResultCallback = callback;
        doExecute(intent, context, callback);
    }

    protected abstract void doExecute(Intent intent, Context context, ResultReceiver callback);

    protected void notifySuccess(Bundle data){
        sendUpdate(RESPONSE_SUCCESS, data);
    }

    protected void notifyFailure(Bundle data){
        sendUpdate(RESPONSE_FAILED, data);
    }

    protected void sendProgress(int progress){
        Bundle b = new Bundle();
        b.putInt(EXTRA_PROGRESS, progress);

        sendUpdate(RESPONSE_PROGRESS, b);
    }

    private void sendUpdate(int resultCode, Bundle data) {
        if(mResultCallback != null){
            mResultCallback.send(resultCode, data);
        }
    }

    public synchronized void cancel(){
        cancelled = true;
    }
}
