package com.eleks.bookroom.data.exchange;

import android.util.Log;

import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.data.structures.Room;
import com.eleks.bookroom.data.Constants;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import microsoft.exchange.webservices.data.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by maryan.melnychuk on 06.12.13.
 */
public class ExchangeHandler {
    private final int timeZoneOffset = SimpleTimeZone.getDefault().getRawOffset();

    private String mUsername, mPassword, mDomain, mServerUrl;

    private ExchangeService mService;
    private List<Room> mRoomList;
    private List<DateTime> mDates;
    private List<List<List<Boolean>>> mRoomStatusList;
    private List<Meeting> mMeetingList;
    private List<AttendeeInfo> mAttendees;
    private String mUserDisplayName;


    public ExchangeHandler(String username, String password, String domain, String serverUrl) {
        mUsername = username;
        mPassword = password;
        mDomain = domain;
        mServerUrl = serverUrl;

        createService();
    }

    private void createService() {
        mService = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        ExchangeCredentials credentials = new WebCredentials(mUsername, mPassword, mDomain);

        mService.setCredentials(credentials);
        URI server = null;
        try {
            server = new URI(mServerUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        mService.setUrl(server);

        mService.setTraceEnabled(Constants.EWS_TRACE_ENABLED);
    }

    private void createUserDisplayName() throws Exception {
        NameResolutionCollection nameResolutions = mService.resolveName(mUsername, ResolveNameSearchLocation.ContactsThenDirectory, true);
        for (NameResolution nameResolution : nameResolutions) {
            mUserDisplayName = nameResolution.getContact().getDisplayName();
        }
    }

    private Interval[] getDayIntervalWithExchangeTime(DateTime day) {
        Interval[] dayIntervalWithExchangeTime = Room.getActualDayIntervals(day);
        for (int i = 0; i < Room.SIZE_OF_DAY_INTERVALS; i++) {
            DateTime start = dayIntervalWithExchangeTime[i].getStart().minus(timeZoneOffset).plusMinutes(15);
            DateTime end = dayIntervalWithExchangeTime[i].getEnd().minus(timeZoneOffset).plusMinutes(15);
            dayIntervalWithExchangeTime[i] = new Interval(start, end);
        }
        return dayIntervalWithExchangeTime;
    }

    private void createListOfRoom() throws Exception {
        NameResolutionCollection nameResolutions = mService.resolveName("room", ResolveNameSearchLocation.DirectoryOnly, true);

        mRoomStatusList = new ArrayList<List<List<Boolean>>>();
        mRoomList = new ArrayList<Room>();
        mAttendees = new ArrayList<AttendeeInfo>();
        for (NameResolution nameResolution : nameResolutions) {
            Room room = new Room();
            room.setName(nameResolution.getMailbox().getName());
            room.setEmail(nameResolution.getMailbox().getAddress());
            mRoomList.add(room);
            mAttendees.add(new AttendeeInfo(nameResolution.getMailbox().getAddress()));
        }
    }

    private void getIntervalsDaysList(Interval interval){
        DateTime start = interval.getStart();
        DateTime end = interval.getEnd();

        mDates = new ArrayList<DateTime>();
        int dayDuration = end.getDayOfMonth() - start.getDayOfMonth();
        mDates.add(start);
        if(dayDuration > 0){
            for(int i = 1; i <= dayDuration; i++){
                mDates.add(start.plusDays(i));
            }
        }
    }

    private void createFreeBusyStatus(Interval interval) throws Exception {
        getIntervalsDaysList(interval);

        Date start = interval.getStart().minus(timeZoneOffset).toDate();
        Date end = interval.getEnd().minus(timeZoneOffset).toDate();

        // Call the availability mService.
        GetUserAvailabilityResults results = mService.getUserAvailability(mAttendees, new TimeWindow(start, end), AvailabilityData.FreeBusy);

        // Output attendee availability information.
        int attendeeIndex = 0;
        Interval meetingInterval;
        boolean isAvailable;
        List<List<Boolean>> daysStatus;
        List<Boolean> timeLineStatus;
        for (AttendeeAvailability attendeeAvailability : results.getAttendeesAvailability()) {
            if (attendeeAvailability.getErrorCode() == ServiceError.NoError) {
                daysStatus = new ArrayList<List<Boolean>>();
                for(DateTime day : mDates){
                    timeLineStatus = new ArrayList<Boolean>();
                    Interval[] dayInterval = getDayIntervalWithExchangeTime(day);
                    for (int i = 0; i < Room.SIZE_OF_DAY_INTERVALS; i++) {
                        isAvailable = true;
                        for (CalendarEvent calendarEvent : attendeeAvailability.getCalendarEvents()) {
                            meetingInterval = new Interval(calendarEvent.getStartTime().getTime(), calendarEvent.getEndTime().getTime());

                            if (meetingInterval.overlap(dayInterval[i]) != null) {
                                isAvailable = false;
                                break;
                            }
                        }
                        timeLineStatus.add(isAvailable);
                    }
                    daysStatus.add(timeLineStatus);
                }
                mRoomStatusList.add(daysStatus);
            }
            attendeeIndex++;
        }
    }

    private void createBookedRoomStatus(int duration) throws Exception {
        Date start = new DateTime(System.currentTimeMillis()).plus(timeZoneOffset).toDate();
        Date end = null;
        switch (duration) {
            case Constants.DURATION_DAY:
                end = new DateTime(start).plusDays(1).toDate();
                break;
            case Constants.DURATION_WEEK:
                end = new DateTime(start).plusWeeks(1).toDate();
                break;
            case Constants.DURATION_MONTH:
                end = new DateTime(start).plusMonths(1).toDate();
                break;
            case Constants.DURATION_TWO_MONTH:
                end = new DateTime(start).plusMonths(2).toDate();
                break;
        }

        // Call the availability mService.
        GetUserAvailabilityResults results = mService.getUserAvailability(mAttendees, new TimeWindow(start, end), AvailabilityData.FreeBusy);

        // Output attendee availability information.

        mMeetingList = new ArrayList<Meeting>();
        for (AttendeeAvailability attendeeAvailability : results.getAttendeesAvailability()) {
            if (attendeeAvailability.getErrorCode() == ServiceError.NoError) {
                for (CalendarEvent calendarEvent : attendeeAvailability.getCalendarEvents()) {
                    if (calendarEvent.getDetails() != null) {
                        if (calendarEvent.getDetails().getSubject().trim().equals(mUserDisplayName)) {
                            CalendarFolder cf = CalendarFolder.bind(mService, WellKnownFolderName.Calendar);
                            FindItemsResults<Appointment> findResults = cf.findAppointments(new CalendarView(calendarEvent.getStartTime(), calendarEvent.getEndTime()));
                            for (Appointment appt : findResults.getItems()) {
                                if (calendarEvent.getDetails().getLocation().equals(appt.getLocation())) {
                                    Meeting meeting = new Meeting();
                                    meeting.setSubject(appt.getSubject());
                                    meeting.setUniqueId(appt.getId().toString());
                                    meeting.setLocation(calendarEvent.getDetails().getLocation());
                                    meeting.setMeetingInterval(new Interval(toUserTime(new DateTime(calendarEvent.getStartTime())), toUserTime(new DateTime(calendarEvent.getEndTime()))));
                                    mMeetingList.add(meeting);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    private DateTime toServerTime(DateTime time){
        return time.minus(timeZoneOffset);
    }

    private DateTime toUserTime(DateTime time){
        return time.plus(timeZoneOffset);
    }

    public synchronized List<Meeting> getUserMeetingsList(int duration) {
        try {
            createListOfRoom();
            createUserDisplayName();
            createBookedRoomStatus(duration);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(Constants.LOG_TAG, "Problem with creating booked rooms list of rooms");
            return null;
        }

        return mMeetingList;
    }

    public synchronized List<Room> getRooms(Interval interval) {
        try {
            createListOfRoom();
            createFreeBusyStatus(interval);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(Constants.LOG_TAG, "Problem with creating quick free/busy list of rooms");
            return null;
        }
        int roomSize = mRoomList.size();
        int datesSize = mDates.size();
        for (int i = 0; i < roomSize; i++) {
            List<List<Boolean>> statusDayRoom = new ArrayList<List<Boolean>>();
            for(int j = 0; j < datesSize; j++){
                mRoomList.get(i).getDates().add(mDates.get(j));


                List<Boolean> dayStatus = mRoomStatusList.get(i).get(j);
                /*dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);
                dayStatus.remove(dayStatus.size() - 1);

                dayStatus.add(0, true);
                dayStatus.add(0, true);
                dayStatus.add(0, true);
                dayStatus.add(0, true);
                dayStatus.add(0, true);
                dayStatus.add(0, true);
                dayStatus.add(0, true);*/

                statusDayRoom.add(dayStatus);

                mRoomList.get(i).setRoomStatus(statusDayRoom);
            }
        }

        return mRoomList;
    }

    public synchronized Meeting getMeeting(Meeting meeting) {
        try {
            Appointment appointment = Appointment.bind(mService, new ItemId(meeting.getUniqueId()));
            AttendeeCollection attendeeCollection = appointment.getRequiredAttendees();
            List<String> invites = new ArrayList<String>();
            for (Attendee attendee : attendeeCollection) {
                invites.add(attendee.getAddress());
            }

            meeting.setSubject(appointment.getSubject());
            meeting.setLocation(appointment.getLocation().trim());
            meeting.setMeetingInterval(new Interval(new DateTime(appointment.getStart()).plus(timeZoneOffset), new DateTime(appointment.getEnd()).plus(timeZoneOffset)));
            meeting.setInvites(invites);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return meeting;
    }

    public synchronized boolean cancelMeetingRequest(Meeting meeting) {
        Appointment appointment = null;
        try {
            appointment = Appointment.bind(mService, new ItemId(meeting.getUniqueId()));
            appointment.cancelMeeting();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean createMeetingRequest(Meeting meeting) {
        try {
            // Create the appointment.
            Appointment appointment = new Appointment(mService);

            // Set properties on the appointment. Add two required mAttendees and one optional attendee.
            appointment.setSubject(meeting.getSubject());
            appointment.setBody(new MessageBody(meeting.getMessageOfBody()));
            appointment.setStart(meeting.getMeetingInterval().getStart().minus(timeZoneOffset).toDate());
            appointment.setEnd(meeting.getMeetingInterval().getEnd().minus(timeZoneOffset).toDate());
            appointment.setLocation(meeting.getLocation());
            for (String email : meeting.getInvites()) {
                appointment.getRequiredAttendees().add(email);
            }

            // Send the meeting request to all mAttendees and save a copy in the Sent Items folder.
            appointment.save();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateMeetingRequest(Meeting meeting) {
        try {
            Appointment appointment = Appointment.bind(mService, new ItemId(meeting.getUniqueId()));

            appointment.setSubject(meeting.getSubject());
            appointment.setBody(new MessageBody(meeting.getMessageOfBody()));
            appointment.setLocation(meeting.getLocation());

            appointment.setStart(meeting.getMeetingInterval().getStart().minus(timeZoneOffset).toDate());
            appointment.setEnd(meeting.getMeetingInterval().getEnd().minus(timeZoneOffset).toDate());

            appointment.getRequiredAttendees().clear();
            for (String address : meeting.getInvites()) {
                appointment.getRequiredAttendees().add(address);
            }

            appointment.update(ConflictResolutionMode.AlwaysOverwrite, SendInvitationsOrCancellationsMode.SendOnlyToAll);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized void getExchangeEmail(List<String> emailList) throws Exception {
        final String[] alphabet = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        String email;
        for (String letter : alphabet) {
            NameResolutionCollection nameResolutions = mService.resolveName(letter, ResolveNameSearchLocation.DirectoryOnly, true);
            for (NameResolution nameResolution : nameResolutions) {
                email = nameResolution.getMailbox().getAddress();
                if (!emailList.contains(email)) {
                    emailList.add(nameResolution.getMailbox().getAddress());
                }
            }
        }

    }

    public synchronized boolean checkConnection() {
        try {
            mService.resolveName("room", ResolveNameSearchLocation.DirectoryOnly, true);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
