package com.eleks.bookroom.ui;

import android.os.Bundle;

/**
 * Created by maryan.melnychuk on 12.12.13.
 */
public interface UpdatedFragment {
    public void updateFragment();
    public Bundle getSharedDate();
}
