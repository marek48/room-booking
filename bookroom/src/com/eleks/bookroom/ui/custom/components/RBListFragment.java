package com.eleks.bookroom.ui.custom.components;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.eleks.bookroom.data.serviceExecutor.RBServiceCallbackListener;
import com.eleks.bookroom.data.serviceExecutor.RBServiceHandler;
import com.eleks.bookroom.data.RBApplication;
import com.eleks.bookroom.ui.UpdatedFragment;

/**
 * Created by maryan.melnychuk on 05.02.14.
 */
public abstract class RBListFragment extends ListFragment implements RBServiceCallbackListener, UpdatedFragment {
    private RBServiceHandler mServiceHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceHandler = RBApplication.getApplication().getServiceHandler();
    }

    @Override
    public void onResume() {
        super.onResume();
        mServiceHandler.addListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mServiceHandler.removeListener(this);
    }

    public RBServiceHandler getServiceHandler() {
        return mServiceHandler;
    }
}
