package com.eleks.bookroom.ui.booked;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Toast;

import com.eleks.bookroom.data.Constants;
import com.eleks.bookroom.R;
import com.eleks.bookroom.data.commands.GetUserMeetingsCommand;
import com.eleks.bookroom.data.commands.RBBaseCommand;
import com.eleks.bookroom.data.structures.Meeting;
import com.eleks.bookroom.ui.custom.components.RBListFragment;
import com.eleks.bookroom.ui.main.MainActivity;
import com.eleks.bookroom.ui.advanced.AdvancedBookingFragment;

import java.util.ArrayList;
import java.util.List;
import static com.eleks.bookroom.data.Constants.*;

/**
 * Created by Administrator on 03.12.13.
 */
public class MyBookedFragment extends RBListFragment {
    private Bundle mSharedData;
    private List<Meeting> mMeetings;
    private MyBookedAdapter mAdapter;
    private int mDuration;
    private int mRequestId = -1;

    public MyBookedFragment(int duration){
        mDuration = duration;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((ActionBarActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        if(getArguments() != null) {
            mSharedData = getArguments().getBundle(SHARED_DATA);
            if(mSharedData != null){
                mMeetings = mSharedData.getParcelableArrayList(MEETING_LIST);
            } else {
                mSharedData = new Bundle();
            }
            Bundle advancedData = getArguments().getBundle(ADVANCED_FREE_BUSY_DATA);
            if(advancedData != null){
                if(advancedData.getInt(TYPE_ADVANCED) == AdvancedBookingFragment.TYPE_EDIT){
                    Meeting meeting = advancedData.getParcelable(MEETING);
                    if(meeting != null){
                        AdvancedBookingFragment fragment = new AdvancedBookingFragment(meeting, mSharedData);
                        fragment.setArguments(getArguments());
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, fragment)
                                .commit();
                    }
                }
            }
            if(mMeetings != null){
                mDuration = ((MainActivity)getActivity()).getDuration();
                showList();
                return;
            }
        }

        getRoomList();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ActivityCompat.invalidateOptionsMenu(getActivity());
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBundle(SHARED_DATA, mSharedData);
    }

    public void setDuration(int duration){
        this.mDuration = duration;
        updateFragment();
    }

    @Override
    public void updateFragment() {
        getRoomList();
    }

    @Override
    public Bundle getSharedDate() {
        Bundle data = new Bundle();
        data.putBundle(SHARED_DATA, mSharedData);
        return data;
    }

    private void getRoomList(){
        mRequestId = getServiceHandler().getUserMeetings(mDuration);

        setListShown(false);
    }

    @Override
    public void onServiceCallback(int requestId, Intent requestIntent, int resultCode, Bundle data) {
        if(getServiceHandler().check(requestIntent, GetUserMeetingsCommand.class)){
            if(resultCode == RBBaseCommand.RESPONSE_SUCCESS){
                mMeetings = data.getParcelableArrayList(MEETING_LIST);
                mSharedData.putParcelableArrayList(MEETING_LIST, (ArrayList)mMeetings);
                showList();
            } else if(resultCode == RBBaseCommand.RESPONSE_FAILED){
                if(Constants.isNetworkOnline(getActivity())){
                    Toast.makeText(getActivity(), R.string.error_connection, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void showList(){
        mAdapter = new MyBookedAdapter(getActivity(), mMeetings, mSharedData, MyBookedFragment.this);
        setListAdapter(mAdapter);
        setListShown(true);
    }
}
